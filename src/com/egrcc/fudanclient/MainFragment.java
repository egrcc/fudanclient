package com.egrcc.fudanclient;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.special.ResideMenu.ResideMenu;

public class MainFragment extends Fragment {

	ImageView imageView1, imageView2, imageView3;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_main, container,
				false);
		imageView1 = (ImageView) rootView.findViewById(R.id.setting);
		imageView1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				getFragmentManager()
				.beginTransaction()
				.replace(R.id.container, new ProfileFragment(),
						"fragment")
				.setTransitionStyle(
						FragmentTransaction.TRANSIT_FRAGMENT_FADE)
				.commit();
			}
		});
		imageView2 = (ImageView) rootView.findViewById(R.id.more);
		imageView2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				getFragmentManager()
						.beginTransaction()
						.replace(R.id.container, new SettingsFragment(),
								"fragment")
						.setTransitionStyle(
								FragmentTransaction.TRANSIT_FRAGMENT_FADE)
						.commit();
			}
		});
		imageView3 = (ImageView) rootView.findViewById(R.id.menu);
		imageView3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				MainActivity.resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
			}
		});
		return rootView;
	}
}