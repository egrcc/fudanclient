package com.egrcc.fudanclient;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.special.ResideMenu.ResideMenu;

public class ProfileFragment extends Fragment{
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View rootView = inflater.inflate(R.layout.profile, container, false);
		rootView.findViewById(R.id.buttons).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
//				getActivity().resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
				
			}
		});
		return rootView;
	}
}