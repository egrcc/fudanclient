package com.egrcc.fudanclient;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.special.ResideMenu.ResideMenu;
import com.special.ResideMenu.ResideMenuItem;

public class MainActivity extends Activity implements OnClickListener {

	public static ResideMenu resideMenu;
	private MainActivity mContext;
	private ResideMenuItem itemHome;
	private ResideMenuItem itemProfile;
	private ResideMenuItem itemCalendar;
	private ResideMenuItem itemSettings;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mContext = this;
		getFragmentManager().beginTransaction()
				.add(R.id.container, new MainFragment()).commit();
		// changeFragment(new MainFragment());
		// if (savedInstanceState == null) {
		// getFragmentManager().beginTransaction()
		// .add(R.id.container, new PlaceholderFragment()).commit();
		// }
		// attach to current activity;
		resideMenu = new ResideMenu(this);
		resideMenu.setBackground(R.drawable.menu_background);
		resideMenu.attachToActivity(this);
		resideMenu.setDirectionDisable(ResideMenu.DIRECTION_RIGHT);

		// create menu items;
		itemHome = new ResideMenuItem(this, R.drawable.icon_home, "Home");
		itemProfile = new ResideMenuItem(this, R.drawable.icon_profile,
				"Profile");
		itemCalendar = new ResideMenuItem(this, R.drawable.icon_calendar,
				"Calendar");
		itemSettings = new ResideMenuItem(this, R.drawable.icon_settings,
				"Settings");

		itemHome.setOnClickListener(this);
		itemProfile.setOnClickListener(this);
		itemCalendar.setOnClickListener(this);
		itemSettings.setOnClickListener(this);

		resideMenu.addMenuItem(itemHome, ResideMenu.DIRECTION_LEFT);
		resideMenu.addMenuItem(itemProfile, ResideMenu.DIRECTION_LEFT);
		resideMenu.addMenuItem(itemCalendar, ResideMenu.DIRECTION_LEFT);
		resideMenu.addMenuItem(itemSettings, ResideMenu.DIRECTION_LEFT);

		findViewById(R.id.title_bar_left_menu).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);

					}
				});
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	// public static class PlaceholderFragment extends Fragment {
	//
	// public PlaceholderFragment() {
	// }
	//
	// ImageView imageView1, imageView2, imageView3;
	//
	// @Override
	// public View onCreateView(LayoutInflater inflater, ViewGroup container,
	// Bundle savedInstanceState) {
	// View rootView = inflater.inflate(R.layout.fragment_main, container,
	// false);
	// imageView1 = (ImageView) rootView.findViewById(R.id.setting);
	// imageView1.setOnClickListener(new OnClickListener() {
	//
	// @Override
	// public void onClick(View v) {
	// // Intent intent = new Intent(getActivity(),
	// // SettingsActivity.class);
	// // startActivity(intent);
	// }
	// });
	// imageView2 = (ImageView) rootView.findViewById(R.id.more);
	// imageView2.setOnClickListener(new OnClickListener() {
	//
	// @Override
	// public void onClick(View v) {
	// // Intent intent = new Intent(getActivity(),
	// // SettingsActivity.class);
	// // startActivity(intent);
	// }
	// });
	// imageView3 = (ImageView) rootView.findViewById(R.id.menu);
	// imageView3.setOnClickListener(new OnClickListener() {
	//
	// @Override
	// public void onClick(View v) {
	// // resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
	// }
	// });
	// return rootView;
	// }
	// }

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		return resideMenu.onInterceptTouchEvent(ev)
				|| super.dispatchTouchEvent(ev);
	}

	@Override
	public void onClick(View view) {

		if (view == itemHome) {
			changeFragment(new MainFragment());
		} else if (view == itemProfile) {
			changeFragment(new ProfileFragment());
		} else if (view == itemCalendar) {
			changeFragment(new CalendarFragment());
		} else if (view == itemSettings) {
			changeFragment(new SettingsFragment());
		}

		resideMenu.closeMenu();
	}

	private void changeFragment(Fragment targetFragment) {
		resideMenu.clearIgnoredViewList();
		getFragmentManager().beginTransaction()
				.replace(R.id.container, targetFragment, "fragment")
				.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
				.commit();
	}

}
